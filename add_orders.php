<?php include "templates/header.php"; ?>

<?php require_once ("dbconfig.php"); 
	
	$id="";
	$repname="";
	$qty="";	
	$total="";
	$paymenttype="";
	$paid="";
	$due="";	

		if(isset($_GET['rep_id'])){
			$id = $_GET['rep_id'];
			$sql="Select * from inv_orders where rep_id=?";
			$res=$conn->prepare($sql);
			$res->execute(array($id));		
	} 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Orders</title>
<style>
.myButton {
	-moz-box-shadow:inset 0px 1px 0px 0px #ffffff;
	-webkit-box-shadow:inset 0px 1px 0px 0px #ffffff;
	box-shadow:inset 0px 1px 0px 0px #ffffff;
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #dfdfdf));
	background:-moz-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-webkit-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-o-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:-ms-linear-gradient(top, #ededed 5%, #dfdfdf 100%);
	background:linear-gradient(to bottom, #ededed 5%, #dfdfdf 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#dfdfdf',GradientType=0);
	background-color:#ededed;
	-moz-border-radius:6px;
	-webkit-border-radius:6px;
	border-radius:6px;
	border:1px solid #dcdcdc;
	display:inline-block;
	cursor:pointer;
	color:#777777;
	font-family:arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #ffffff;
}
.myButton:hover {
	background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #dfdfdf), color-stop(1, #ededed));
	background:-moz-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
	background:-webkit-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
	background:-o-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
	background:-ms-linear-gradient(top, #dfdfdf 5%, #ededed 100%);
	background:linear-gradient(to bottom, #dfdfdf 5%, #ededed 100%);
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#dfdfdf', endColorstr='#ededed',GradientType=0);
	background-color:#dfdfdf;
}
.myButton:active {
	position:relative;
	top:1px;
}
</style>
</head>
<?php
	$sqladd="Select * from inv_orders where rep_id=?";
	$resadd=$conn->prepare($sqladd);
	$resadd->execute(array($id));
		while($rowadd = $resadd->fetch(PDO::FETCH_ASSOC)){
		$id=$rowadd['rep_id'];
		$catname=$rowadd['rep_name'];
		$parent=$rowadd['qty'];		
	}
?>
    <form method="post" name="frmStudent" action="save_category.php">
    <input type="hidden" name="cat_id" value="<?php echo $id; ?>"/>
        <table>
            <tr><td>Category Name</td><td>:</td><td><input type="text" name="catname" required="required" value="<?php echo $catname; ?>"/></td></tr>
            <tr><td>Parent</td><td>:</td><td><input type="text" name="parent" required="required" value="<?php echo $parent; ?>"/></td></tr>
            
            <tr><td></td><td></td><td><input type="submit" class="myButton" value="Save"/></td></tr>
        </table>
    </form>
<body>
</body>
</html>
