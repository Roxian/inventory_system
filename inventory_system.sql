-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 07, 2018 at 06:58 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `inv_brands`
--

CREATE TABLE `inv_brands` (
  `brand_id` int(11) NOT NULL,
  `brandname` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_brands`
--

INSERT INTO `inv_brands` (`brand_id`, `brandname`, `status`) VALUES
(1, 'Nokia', ''),
(2, 'Oppo', ''),
(3, 'Samsung', ''),
(4, 'Apple', '');

-- --------------------------------------------------------

--
-- Table structure for table `inv_category`
--

CREATE TABLE `inv_category` (
  `cat_id` int(11) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `parent` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_category`
--

INSERT INTO `inv_category` (`cat_id`, `catname`, `parent`, `status`) VALUES
(3, 'Furniture', 'Sala', ''),
(5, 'Electronics', 'Cellphone', ''),
(6, 'Foods', 'Can Goods', ''),
(7, 'Softwares', 'Root', '');

-- --------------------------------------------------------

--
-- Table structure for table `inv_orders`
--

CREATE TABLE `inv_orders` (
  `rep_id` int(11) NOT NULL,
  `rep_name` varchar(255) NOT NULL,
  `qty` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `paid` int(11) NOT NULL,
  `due` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inv_products`
--

CREATE TABLE `inv_products` (
  `prod_id` int(11) NOT NULL,
  `prod_name` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `instock` int(11) NOT NULL,
  `price` double NOT NULL,
  `model_number` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_products`
--

INSERT INTO `inv_products` (`prod_id`, `prod_name`, `status`, `instock`, `price`, `model_number`, `cat_id`, `brand_id`) VALUES
(1, 'Iphone 5c', '', 1, 4990, 1111, 0, 0),
(2, 'Young\'s Town', '', 1000, 5, 1234567, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `inv_user`
--

CREATE TABLE `inv_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inv_user`
--

INSERT INTO `inv_user` (`user_id`, `username`, `email`, `password`) VALUES
(1, 'admin', 'admin@admin.com', 'admin'),
(2, 'Boss Anne', 'oleafaithroxanne7853@gmail.com', 'bossanne'),
(3, 'Nat', 'tawingarpon@hjgh.com', '123456'),
(4, 'admin', 'admin@nothing.org', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `inv_brands`
--
ALTER TABLE `inv_brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `inv_category`
--
ALTER TABLE `inv_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `inv_orders`
--
ALTER TABLE `inv_orders`
  ADD PRIMARY KEY (`rep_id`);

--
-- Indexes for table `inv_products`
--
ALTER TABLE `inv_products`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `inv_user`
--
ALTER TABLE `inv_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `inv_brands`
--
ALTER TABLE `inv_brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `inv_category`
--
ALTER TABLE `inv_category`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `inv_orders`
--
ALTER TABLE `inv_orders`
  MODIFY `rep_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `inv_products`
--
ALTER TABLE `inv_products`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `inv_user`
--
ALTER TABLE `inv_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
